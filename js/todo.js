$(function() {

    $(document).ready(function(){
        $('.sidenav').sidenav();
    });


    var static_id=0;
    var type = "all";
    var gen = 0;
    listePok = [];
    $("#add").on("click", true,pagePokemon);
    $("#acc").on("click", refreshList);
    $(".type").click(function(){type = $(this).attr('id');});
    $(".gen").click(function(){gen = parseInt($(this).attr('id'));});
    $("#filter").click(tri);

     function Pokemon(nom, num, t1, t2, pv, att, def, vit, hei, wei, img, id, uri="http://localhost:3000/Pokemon/"+id){
        this.Numero = num;
        this.Nom    = nom;
        this.Type1  = t1;
        this.Type2  = t2;
        this.PV     = pv;
        this.Attaque= att;
        this.Defense= def;
        this.Vitesse= vit;
        this.Taille = hei;
        this.Poids  = wei;
        this.Image  = img.length > 7 ? img : "img/"+num+".png" ;
        this.uri    = uri
        this.id     = id;
    }


    function refreshList(){
        $("#currentpokemon").empty();
        $.ajax({
            url: "http://localhost:3000/pokemon",
            type: "GET",
            dataType : "json",
            success: function(pokemon) {
                listePok = pokemon;
                $('#taches').empty();
                $('#taches').append($('<div class="card-container">'));
                for(let i=0;i<pokemon.length;i++){
                    static_id = i;                  
                    $('#taches .card-container')
                    .append($('<div class="card">')
                        .append($('<div data-target="slide-out" class="sidenav-trigger card-image waves-effect waves-block waves-light">').append($('<img class="activator" src='+(pokemon[i].Image.length > 7 ? pokemon[i].Image : "img/"+pokemon[i].Image)+'>').append($('</div>'))))
                        .append($('<div class="card-content">').append($('<span class="card-title activator grey-text text-darken-4">'+pokemon[i].Nom+'</span>').append($('</div>'))))
                    .append($('</div>')).on("click", pokemon[i], details));
                }
                $('#taches').append($('</div>'));
                static_id++;
                    },
            error: function(req, status, err) {
                        $("#taches").html("<b>Impossible de récupérer vos pokémons</b>");
                        }
                        });
        }

    function details(event){
        console.log(0)
        pagePokemon();
        var pok = new Pokemon(event.data["Nom"],
                              event.data["Numero"],
                              event.data["Type1"],
                              event.data["Type2"],
                              event.data["PV"],
                              event.data["Attaque"],
                              event.data["Defense"],
                              event.data["Vitesse"],
                              event.data["Taille"],
                              event.data["Poids"],
                              event.data["Image"],
                              event.data["id"]
                                );
        afficheDetails(pok);
        }

   

    function pagePokemon(isnew){
        $("#currentpokemon").empty();
        if (isnew) {
            $("#currentpokemon").append($("<span>URL de l'image<br><input type=text id='ImgNew'><br></span>"))
        } else {
            $("#currentpokemon")
            .append($('<span><img height=350 width=350 id="Img"><br></span>'))
            .append($('<span>Numéro Pokedex<br><input type="text" id="Num" readonly ><br><br></span>'))
        }
        $("#currentpokemon")
            .append($('<span>Nom<br><input type="text" id="Nom"><br></span>'))
            .append($('<span>Type 1<br><input type="text" id="Type1"><br></span>'))
            .append($('<span>Type 2<br><input type="text" id="Type2"><br></span>'))
            .append($('<span>PV<br><input type="number" id="PV"><br></span>'))
            .append($('<span>Attaque<br><input type="number" id="Att"><br></span>'))
            .append($('<span>Défense<br><input type="number" id="Def"><br></span>'))
            .append($('<span>Vitesse<br><input type="number" id="Vit"><br></span>'))
            .append($('<span>Taille<br><input step=0.1 type="number" id="Taille"><br></span>'))
            .append($('<span>Poids<br><input step=0.1 type="number" id="Poids"><br></span>'))
            .append($('<span><input type="hidden" id="uri"><br></span>'))
            .append($('<span><input type="hidden" id="id"><br></span>'))
            if (isnew) {
                $("#currentpokemon").append($('<span><i class="large material-icons">save</i></span>').on("click", saveNewPokemon));
            }else{
                $("#currentpokemon")
                .append($('<span><i class="large material-icons">edit</i></span>').on("click", savePokemon))
                .append($('<span><i class="large material-icons">delete</i><br></span>').on("click", deletePokemon));
            } 
        }

    function afficheDetails(t){

        var img = document.getElementById("Img");
        img.src = t.Image;

        var num = document.getElementById("Num");
        num.value = t.Numero;

        var nom = document.getElementById("Nom");
        nom.value = t.Nom;

        var type1 = document.getElementById("Type1");
        type1.value = t.Type1;

        var type2 = document.getElementById("Type2");
        type2.value = t.Type2;

        var pv = document.getElementById("PV");
        pv.value = t.PV;

        var att = document.getElementById("Att");
        att.value = t.Attaque;

        var def = document.getElementById("Def");
        def.value = t.Defense;

        var vit = document.getElementById("Vit");
        vit.value = t.Vitesse;

        var taille = document.getElementById("Taille");
        taille.value = t.Taille;

        var poids = document.getElementById("Poids");
        poids.value = t.Poids;

        var uri = document.getElementById("uri");
        uri.value = t.uri;

        var id = document.getElementById("id");
        id.value = t.id;
    }

    function saveNewPokemon(){
        static_id++;
        var numero = "";
        if (static_id<10) {numero = "00"+static_id;} else if(static_id < 100) {numero = "0"+static_id;} else {numero = ""+static_id;}
        var pok = new Pokemon(  $("#currentpokemon #Nom").val(),
                                numero,
                                $("#currentpokemon #Type1").val(),
                                $("#currentpokemon #Type2").val(),
                                $("#currentpokemon #PV").val(),
                                $("#currentpokemon #Att").val(),
                                $("#currentpokemon #Def").val(),
                                $("#currentpokemon #Vit").val(),
                                $("#currentpokemon #Taille").val(),
                                $("#currentpokemon #Poids").val(),
                                $("#currentpokemon #ImgNew").val() == "" ? "img/"+numero+".png" : $("#currentpokemon #ImgNew").val(),
                                static_id);
        /*console.log(JSON.stringify(pok));*/

        $.ajax({
            url: "http://localhost:3000/Pokemon",
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(pok),
            dataType: 'json',
            success: function(msg){
                alert("Votre pokemon à bien été crée");
                refreshList();
            },
            error: function(err){
                alert("Votre pokemon n'as pas pu être crée");
            }
        });
        
    }



    function savePokemon(){
        var pok = new Pokemon(  $("#currentpokemon #Nom").val(),
                                $("#currentpokemon #Num").val(),
                                $("#currentpokemon #Type1").val(),
                                $("#currentpokemon #Type2").val()=="Aucun" ? "" : $("#currentpokemon #Type2").val(),
                                parseInt($("#currentpokemon #PV").val()),
                                parseInt($("#currentpokemon #Att").val()),
                                parseInt($("#currentpokemon #Def").val()),
                                parseInt($("#currentpokemon #Vit").val()),
                                parseInt($("#currentpokemon #Taille").val()),
                                parseInt($("#currentpokemon #Poids").val()),
                                $("#currentpokemon #Img").attr('src'),
                                $("#currentpokemon #id").val(),
                                $("#currentpokemon #uri").val());
        /*console.log(JSON.stringify(pok));*/
        $.ajax({
            url: pok.uri,
            type: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(pok),
            dataType: 'json',
            success: function(msg){
                alert("Les modifications ont été enregistré");
                refreshList();
            },
            error: function(err){
                alert("Les modifications n'ont pas pu être sauvegardé");
            }
        });
    }

    function deletePokemon(){
         var uri = document.getElementById("uri");

         fetch(uri.value, {method: 'DELETE'})
         .then(reponse => {
            if (reponse.ok) return reponse.status;
            else throw new Error(reponse.status)
         })
         .then(rep => alert("Suppression réussie"))
         .catch(rep => alert(rep.status))
        }


    function displayList(pokemon){
        $("#currentpokemon").empty();
        $('#taches').empty();
        $('#taches').append($('<div class="card-container">'));
        for(let i=0;i<pokemon.length;i++){
            $('#taches .card-container')
            .append($('<div class="card">')
                .append($('<div data-target="slide-out" class="sidenav-trigger card-image waves-effect waves-block waves-light">').append($('<img class="activator" src='+(pokemon[i].Image.length > 7 ? pokemon[i].Image : "img/"+pokemon[i].Image)+'>').append($('</div>'))))
                .append($('<div class="card-content">').append($('<span class="card-title activator grey-text text-darken-4">'+pokemon[i].Nom+'</span>').append($('</div>'))))
            .append($('</div>')).on("click", pokemon[i], details));
            }
        $('#taches').append($('</div>'));
        }


    function tri() {
/*        console.log(type)
        console.log(gen)
        console.log(listePok)*/
        var res = listePok;
        var resTmp = [];
        if (gen != 0) {
            for(p of res){
                if (p["Generation"] == gen){resTmp.push(p);}
            }
            res = resTmp;
            resTmp = [];

        }
        if (type != "all") {
            for(p of res){
                if (p["Type1"] == type || p["Type2"] == type){resTmp.push(p);}
            }
            res = resTmp;

        }   
        displayList(res);
    }
refreshList();
});